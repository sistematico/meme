<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Meme</title>
    <meta name="description" content="Meme">
    <meta name="keywords" content="Meme">
    <meta name="author" content="Lucas Saliés Brum">
    <meta property="og:title" content="Meme" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://meme.lucasbrum.net" />
    <meta property="og:image" content="img/meme.png" />
    <link rel="stylesheet" href="css/bulma.min.css">
    <link rel="stylesheet" href="css/fontawesome-5.11.2.min.css">
    <link rel="stylesheet" href="css/bulma-switch.min.css">
    <link rel="stylesheet" href="css/principal.css">
    <link rel="shortcut icon" href="img/favicon.ico">
</head>
<body>
    <section class="hero is-info is-fullheight">
        <div class="hero-body">

            <div class="container">

                <div class="columns is-centered">
                    <div class="column is-half">

                        <div class="tile is-ancestor">
                            <div class="tile is-parent">
                                <article class="tile is-child">
                                    <p class="title">Meme</p>
                                    <p class="subtitle">Busque seu meme</p>
                                    <form enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
                                        <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
                                        <div class="field has-addons">
                                            <div class="control">
                                                <input name="meme" id="meme" class="input" type="text"  placeholder="Nome ou palavra-chave">
                                            </div>
                                            <div class="control">
                                                <input name="meme" id="meme" class="input" type="text"  placeholder="Nome ou palavra-chave">
                                            </div>
                                        </div>
                                        <div class="field has-addons">
                                            <div class="control">
                                                <input name="meme" id="meme" class="input" type="file"  placeholder="Nome ou palavra-chave">
                                            </div>
                                            <div class="control">
                                                <input class="button is-success" type="submit" name="enviar" value="Enviar" />
                                            </div>
                                        </div>
                                    </form>
                                    <br />
                                    <p class="is-size-7 is-italic">
                                        Feito por <a href="https://lucasbrum.net">Lucas Saliés Brum</a>, código fonte no
                                        <a href="https://gitlab.com/sistematico/meme">GitLab</a>.
                                    </p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/script.js"></script>
</body>

</html>