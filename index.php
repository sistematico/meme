<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Meme</title>

    <meta name="description" content="Meme">
    <meta name="keywords" content="Meme">
    <meta name="author" content="Lucas Saliés Brum">

    <meta property="og:title" content="Meme" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://meme.lucasbrum.net" />
    <meta property="og:image" content="img/meme.png" />

    <link rel="stylesheet" href="css/fontawesome-5.11.2.min.css">
    <link rel="stylesheet" href="css/bootstrap4-neon-glow.min.css">
    <!-- <link rel="stylesheet" href="css/principal.css"> -->

    <link rel="shortcut icon" href="img/favicon.ico">
</head>
<body>
    <div class="navbar-dark text-white">
        <div class="container">
            <nav class="navbar px-0 navbar-expand-lg navbar-dark">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <a href="index.html" class="pl-md-0 p-3 text-light">Início</a>
                        <a href="app.html" class="p-3 text-decoration-none text-light">App example</a>
                        <a href="form.html" class="p-3 text-decoration-none text-light">Form example</a>
                    </div>
                </div>
            </nav>
        </div>
    </div>

    <div class="jumbotron bg-transparent mb-0 radius-0">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <h1 class="display-2">Mem<span class="vim-caret">e</span></h1>
                    <div class="lead mb-3 text-mono text-success">Busque os memes mais divertidos da internet.</div>
                    <div class="text-mono">
                        <a href="#!" title="Meme Aleatório" class="btn btn-success btn-shadow px-3 my-2 ml-0 text-left">
                            Meme Aleatório
                        </a>
                        <a href="#!" class="btn btn-danger btn-shadow px-3 my-2 ml-0 ml-sm-1 text-left">
                            Adicionar Meme
                        </a>
                    </div>

                    <p class="mt-5 text-grey text-spacey">
                        <!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto, dignissimos provident.
                        Alias, aliquid,
                        cum cumque deleniti dignissimos eos exercitationem explicabo illum inventore laboriosam nihil
                        nobis nostrum
                        praesentium recusandae sequi, sint! -->
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Ex. John Travolta">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-primary">Buscar</button>
                            </div>
                        </div>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="container py-5">
        <h1>Obrigado</h1>
        <p>Obrigado por usar meu site, você pode contribuir melhorando o código no: <a href="https://gitlab.com/sistematico/meme">GitLab</a>
        </p>
    </div>
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/clipboard.min.js"></script>
    <script src="js/script.js"></script>
</body>
</html>