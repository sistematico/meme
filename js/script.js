$(function() {

    if(localStorage.getItem('hash')) {
        $("#hash").val(localStorage.getItem('hash'));
    }

    var clipboard = new ClipboardJS('.copiar');

    $("#formulario").submit(function(e) {
        e.preventDefault();

        $.ajax({
            url: $("#formulario").attr('action'),
            type: 'post',
            data: $("#formulario").serialize(),
            dataType: 'text',
            success: function(data) {
                $("#hash").val(data);
                localStorage.setItem('hash', data);
            }
        });
    });
});